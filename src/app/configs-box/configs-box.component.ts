import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-configs-box',
  templateUrl: './configs-box.component.html',
  styleUrls: ['./configs-box.component.css']
})
export class ConfigsBoxComponent implements OnInit {
  places = [
    {id: 1, name: 'here'},
    {id: 2, name: 'home'}
  ];
	@Output() deliveryType: EventEmitter<number> = new EventEmitter();
  constructor(private router: Router) { }

  ngOnInit() {
  }
  changeType (type) {
    this.deliveryType.emit(type);
    this.router.navigate(["/place-selection", type]);
  }
}
