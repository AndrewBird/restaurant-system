import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigsBoxComponent } from './configs-box.component';

describe('ConfigsBoxComponent', () => {
  let component: ConfigsBoxComponent;
  let fixture: ComponentFixture<ConfigsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConfigsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfigsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
