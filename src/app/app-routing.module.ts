import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FoodOrderingComponent } from './food-ordering/food-ordering.component';
import { PlaceSelectionComponent } from './place-selection/place-selection.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'place-selection/here',
    pathMatch: 'full'
  },
  {
    path: 'place-selection/:type',
    component: PlaceSelectionComponent,
    children: []
  },
  {
    path: 'place-selection',
    redirectTo: 'place-selection/here',
    pathMatch: 'full'
  },
  {
    path: 'food-ordering/:payment',
    component: FoodOrderingComponent,
    children: []
  },
  {
    path: 'food-ordering',
    component: FoodOrderingComponent,
    children: []
  },
  {
    path: '**',
    component: PlaceSelectionComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class AppRoutingModule { }