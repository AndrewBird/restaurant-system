import { Component, OnInit, ViewChild } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-customer-info',
  templateUrl: './customer-info.component.html',
  styleUrls: ['./customer-info.component.less']
})
export class CustomerInfoComponent implements OnInit {
  
  constructor(public modalRef: BsModalRef) { }
  public decision: string;

  ngOnInit() {

  }
  @ViewChild('customerInfo') customerInfo;

  customerInfoModal = {
    name: "",
    email: "",
    number: null,
    zip: null,
    companyName: "",
    anniversary: null,
    childsBirdthday: "",
    address1: "",
    address2: "",
    birdthday: null,
    spose: "",
    city: ""
  }
  
}
