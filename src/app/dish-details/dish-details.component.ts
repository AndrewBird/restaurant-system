import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { InventoryModalComponent } from '../inventory-modal/inventory-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Dish } from '../dish';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'dish-details',
  templateUrl: './dish-details.component.html',
  styleUrls: ['./dish-details.component.less']
})
export class DishDetailsComponent implements OnInit {

  @Input() dish: Dish;
	@Output() editDish: EventEmitter<any> = new EventEmitter();
	@Output() hide: EventEmitter<any> = new EventEmitter();
  public modalRef: BsModalRef;
  public subscriptions: Subscription[] = [];
  phase: number = 1;
  isPercentsDiscount: boolean;
  constructor(private modalService: BsModalService) { }

  ngOnInit() {
  }
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
  showInventoryModal (dish: Dish) {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {

      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(InventoryModalComponent, {class: 'common-modal'});
    this.modalRef.content.decision = "";
    this.modalRef.content.dish = dish;
  }
  editDishDetails (dish, form) {
    var newDetails = form.value;
    this.phase = 2;
    this.editDish.emit(Object.assign({discountUnits: (this.isPercentsDiscount ? "%" : "$")}, form.value));
  }
  hideDishDetails () {
    this.hide.emit();
    this.phase = 1;
  }

}
