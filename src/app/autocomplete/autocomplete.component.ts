import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { Messenger } from '../messenger.service';

@Component({
  selector: 'autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.less']
})
export class AutocompleteComponent implements OnInit {
  @Input() artefacts;

	@Output() action: EventEmitter<any> = new EventEmitter();
  message: any = {text: ''};
  subscription: Subscription;

  constructor(private messageService: Messenger) {
      this.subscription = this.messageService.getMessage().subscribe(message => {
        this.message = message;
        if (this.artefacts.way && this.artefacts.way.length) {
          this.items = this.analyse(this.artefacts.data, message.text);
        } else {
          this.items = [...this.artefacts.data];
        }
      });
  }
  ngOnInit() {
    this.analyse(this.artefacts.data, this.message.text);
  }
  add () {
    this.action.emit(this.items.filter(item => item.active === true));
  }
  analyse (data, searchText) {
    var itemLayer = [...data],
        suitableItems = [];
    
    this.artefacts.way.forEach(artefact => {
      if (typeof artefact === 'string') {
        itemLayer = this.walk(itemLayer, artefact);
      }
    });
    itemLayer.forEach(item => {
      let it = {text: item, active: false};
      if (item.toLowerCase().indexOf(searchText.toLowerCase()) + 1) {
        suitableItems.push(it);
      }
    });

    return suitableItems;
  }
  walk (entity, hook) {
    var innerEntity = [], assembled = [];

    if (Array.isArray(entity)) {
      let buffer = [];
      entity.forEach(layer => {
        buffer = [...buffer, ...this.walk(layer, hook)];
      });
      innerEntity = buffer;
    } else {
      innerEntity = entity[hook];
    }

    return innerEntity;
  }
  items = []
  ngOnDestroy() {
      this.subscription.unsubscribe();
  }
}
