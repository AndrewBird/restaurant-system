import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignStaffModalComponent } from './assign-staff-modal.component';

describe('AssignStaffModalComponent', () => {
  let component: AssignStaffModalComponent;
  let fixture: ComponentFixture<AssignStaffModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignStaffModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignStaffModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
