import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-assign-staff-modal',
  templateUrl: './assign-staff-modal.component.html',
  styleUrls: ['./assign-staff-modal.component.less']
})
export class AssignStaffModalComponent implements OnInit {
  placesToAssaign: any[] = [];
  unitsStr: string;
  decision: string;
  assigny: string;
  assignStaff: any = {};

  constructor(public modalRef: BsModalRef) { }

  ngOnInit() {
  }

}
