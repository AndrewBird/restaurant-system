import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { TemplateRef } from '@angular/core';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule }   from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { DemoModalServiceStaticComponent } from './demo-modal-service-static/demo-modal-service-static.component';
import { ConfigsBoxComponent } from './configs-box/configs-box.component';
import { SelectsBoxComponent } from './selects-box/selects-box.component';
import { OptionsBoxComponent } from './options-box/options-box.component';
import { MapComponent } from './map/map.component';
import { LoginFormComponent } from './login-form/login-form.component';

import { HttpClientModule } from '@angular/common/http';
import { AgmCoreModule } from '@agm/core';
import { PaymentComponent } from './payment-component/payment-component';
import { FoodOrderingComponent } from './food-ordering/food-ordering.component';
import { PlaceSelectionComponent } from './place-selection/place-selection.component';
import { BasketComponent } from './basket/basket.component';
import { MenuComponent } from './menu/menu.component';
import { AssignStaffModalComponent } from './assign-staff-modal/assign-staff-modal.component';
import { CoversModalComponent } from './covers-modal/covers-modal.component';
import { StateGuardService } from './state-guard.service';
import { DraggableDishDirective } from './draggable-dish.directive';
import { DragService } from './drag-service.service';
import { DropDishDirective } from './drop-dish.directive';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { KeyboardComponent } from './keyboard/keyboard.component';
import { StopPropagationDirective } from './stop-propagation.directive';
import { InventoryModalComponent } from './inventory-modal/inventory-modal.component';
import { GiftCardModalComponent } from './gift-card-modal/gift-card-modal.component';
import { OTModalComponent } from './otmodal/otmodal.component';
import { DishDetailsComponent } from './dish-details/dish-details.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { Messenger } from './messenger.service';
import { ParkModalComponent } from './park-modal/park-modal.component';
import { OffersModalComponent } from './offers-modal/offers-modal.component';
import { ScreenKeysDirective } from './screen-keys.directive';
import { ScreenKeysComponent } from './screen-keys/screen-keys.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { DiscountsModalComponent } from './discounts-modal/discounts-modal.component';
import { FocusDirective } from './focus.directive';

@NgModule({
  declarations: [
    AppComponent,
    DemoModalServiceStaticComponent,
    ConfigsBoxComponent,
    SelectsBoxComponent,
    OptionsBoxComponent,
    LoginFormComponent,
    MapComponent,
    PaymentComponent,
    FoodOrderingComponent,
    PlaceSelectionComponent,
    BasketComponent,
    MenuComponent,
    AssignStaffModalComponent,
    CoversModalComponent,
    DraggableDishDirective,
    DropDishDirective,
    KeyboardComponent,
    StopPropagationDirective,
    InventoryModalComponent,
    GiftCardModalComponent,
    OTModalComponent,
    DishDetailsComponent,
    AutocompleteComponent,
    ParkModalComponent,
    OffersModalComponent,
    ScreenKeysDirective,
    ScreenKeysComponent,
    CustomerInfoComponent,
    DiscountsModalComponent,
    FocusDirective
  ],
  entryComponents: [LoginFormComponent,
                    FoodOrderingComponent,
                    PlaceSelectionComponent,
                    AssignStaffModalComponent,
                    CoversModalComponent, 
                    BasketComponent,
                    KeyboardComponent,
                    InventoryModalComponent,
                    GiftCardModalComponent,
                    OTModalComponent,
                    DishDetailsComponent,
                    AutocompleteComponent,
                    ParkModalComponent,
                    OffersModalComponent,
                    CustomerInfoComponent,
                    DiscountsModalComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AccordionModule.forRoot(),
    CarouselModule.forRoot(),
    ModalModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD5uxLAqmz1XQOBSvp-ATy1LMgzjrL9Vi8'
    }),
    AppRoutingModule,
    TabsModule.forRoot()
  ],
  providers: [StateGuardService, DragService, Messenger],
  bootstrap: [AppComponent]
})
export class AppModule { }
