import { Component, OnInit, ViewChild } from '@angular/core';
import { StateGuardService } from '../state-guard.service';
import { Router } from '@angular/router';
import { InventoryModalComponent } from '../inventory-modal/inventory-modal.component';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Subscription } from 'rxjs/Subscription';
import { BsModalService } from 'ngx-bootstrap/modal';
import { Dish } from '../dish';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.less']
})
export class BasketComponent implements OnInit {
  locations;
  currentUnit;
  stagedVisitor;
  currentLocation;
  public modalRef: BsModalRef;
  public subscriptions: Subscription[] = [];
  private state;
  private moreOptionsListModel = [
    {text: "Edit", callback: "runEdit", active: true},
    {text: "Move Items", callback: "", active: false},
    {text: "Combine Seat", callback: "", active: false},
    {text: "Single Checkout", callback: "addToSingleCheckout", active: false},
    {text: "Move All Item To Seat", callback: "", active: false},
    {text: "Cancel Order", callback: "", active: false},
    {text: "Note", callback: "", active: false}
  ]
  @ViewChild('dishDetailsForm') dishDetailsForm;
  constructor(private stateGuard: StateGuardService, private router: Router, private modalService: BsModalService) { }
  currency: string;
  ngOnInit() {
    this.locations = this.stateGuard.obtain("selects-box.component:places") || [];
    this.currency = this.stateGuard.obtain("app").currency;
    this.currentUnit = this.stateGuard.obtain("selects-box.component:currentUnit") || [];
    this.locations.forEach(location => {
      location.units.forEach(unit => {
        if (unit === this.currentUnit) {
          this.currentLocation = location;
        }
      });
    });
    this.state = {
      moreOptionsMap: new Array(this.currentUnit.visitors.length).fill(false)
    }
  }
  addToSingleCheckout (visitor) {
    if (visitor.isOnHold) { return; }
    var offUnit = Object.assign({}, this.currentUnit); 
    offUnit.visitors = [visitor];

    this.stateGuard.amend('basket.component:offUnits', function (offUnits) {
      var newOffUnit = offUnit;
      offUnits.push(offUnit);
    });
    visitor.isOnHold = true;
    this.currentUnit.visitors.splice(this.currentUnit.visitors.indexOf(visitor), 1);
  }
  filtrate(array, feature) {
    return array.filter(item => item.course === feature);
  }
  toggleMoreOptions (event, visitorIndex) {
    var target = event.target, callback, currentItemState = this.state.moreOptionsMap[visitorIndex];
    if (target !== event.currentTarget) {
      while (!target.classList.contains("option")) {
        target = target.parentElement;
      }
      callback = target.getAttribute("id");
      if (callback) {
        this[callback](this.currentUnit.visitors[visitorIndex]);
      }
    }
    this.state.moreOptionsMap.forEach((item, optionIndex) => {this.state.moreOptionsMap[optionIndex] = false});
    this.state.moreOptionsMap[visitorIndex] = !currentItemState;
  }
  runEdit (visitor) {

  }
  getVisitorsOrderPrice (visitor) {
    var price = 0;
    visitor.dishes.forEach(course => {
      course.forEach(dish => {price += dish.price * dish.amount});
    });
    return price;
  }
  getItemsAmount ( ) {
    var amount = 0;
    this.currentUnit.visitors.forEach(visitor => {
      visitor.dishes.forEach(course => {
        course.forEach(dish => {amount += dish.amount});
      });
    });
    return amount;
  }
  getGeneralPrice ( ) {
    var price = 0;
    this.currentUnit.visitors.forEach(visitor => {
      visitor.dishes.forEach(course => {
        course.forEach(dish => {price += dish.price * dish.amount});
      });
    });
    return price;
  }
  checkPrint() {
    this.currentUnit.activePhase.name = 'blue';
    setTimeout(() => { this.router.navigate(['/place-selection']) }, 0);
  }
  addToBasket(dish) {
    if (this.stagedVisitor) {
      this.stagedVisitor.dishes[0].push(dish);
    }
  }
  onDrop(parcel, course) {
    var address = parcel.address, source;

    course.push(parcel.dish);
    if (address) {
      source = this.currentUnit.visitors[address[0]].dishes[address[1]];
      source.splice(address[2], 1);
    }
  }
  addToVisitorsMeal (visitor, dish) {
    
  }
  toggleVisitor(visitor) {
    var newState = !visitor.on;
    /*this.locations.forEach(location => {
      location.units.forEach(unit => {
        if (unit.visitors) {
          unit.visitors.forEach(visitor => {
            visitor.on = false;
          });
        }
      });
    });*/

    visitor.on = newState;
  }
  removeTheDish(visitorIndex, courseIndex, dishIndex) {
    this.currentUnit.visitors[visitorIndex].dishes[courseIndex].splice(dishIndex, 1);
  }
  toggleDishDetails(event, dish) {
    event.stopPropagation();
    dish.expanded = !dish.expanded;
  }
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
  editDishDetails (parcel, dish) {
    var newDetails = parcel;
    dish.amount = newDetails.quantity || dish.amount;
    dish.price = newDetails.price || dish.price;
    dish.discount = newDetails.discount || dish.discount;
    dish.discountUnits = parcel.discountUnits;
    dish.note = newDetails.note;
  }
}