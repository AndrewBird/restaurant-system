import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceSelectionComponent } from './place-selection.component';

describe('PlaceSelectionComponent', () => {
  let component: PlaceSelectionComponent;
  let fixture: ComponentFixture<PlaceSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
