import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-place-selection',
  templateUrl: './place-selection.component.html',
  styleUrls: ['./place-selection.component.less']
})
export class PlaceSelectionComponent implements OnInit {
  placeType: string = 'here';
  @Input() deliveryType: number;
  constructor(private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.placeType = this.route.snapshot.params.type || "here";
  }
  getCurrentPlaceType () {
    this.placeType = this.route.snapshot.params.type;
    return this.route.snapshot.params.type;
  }
  updateState(parcel) {
    this.placeType = parcel;
  }
  proceedToFoodOrdering () {
    this.router.navigate(["/food-ordering"]);
  }
}
