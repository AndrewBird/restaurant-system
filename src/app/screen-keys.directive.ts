import { Directive, HostListener } from '@angular/core';
import { StateGuardService } from './state-guard.service';

@Directive({
  selector: '[screen-keys]'
})
export class ScreenKeysDirective {

  constructor(private stateGuard: StateGuardService) { }

  @HostListener('focus', ['$event'])
  
  onFocus (event) {
    this.stateGuard.entrust("screen-keys.directive:currentInput", event.srcElement);
  }
}
