import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiftCardModalComponent } from './gift-card-modal.component';

describe('GiftCardModalComponent', () => {
  let component: GiftCardModalComponent;
  let fixture: ComponentFixture<GiftCardModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiftCardModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiftCardModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
