import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-otmodal',
  templateUrl: './otmodal.component.html',
  styleUrls: ['./otmodal.component.less']
})
export class OTModalComponent implements OnInit {
  decision: string = '';
  unit: Unit = {visitors: []};
  choosensMap = new Array(this.unit.visitors.length).fill(false);
  constructor(public modalRef: BsModalRef) { }
  getResult () {
    var choosenVisitors = this.unit.visitors.filter((seat, index) => {
      return this.choosensMap[index] ? seat : false;
    });
    return {
      choosenVisitors: choosenVisitors,
      unit: this.unit
    }
  }
  ngOnInit() {
  }

}
export interface Unit {
  visitors
}
