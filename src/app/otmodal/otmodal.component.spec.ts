import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OTModalComponent } from './otmodal.component';

describe('OTModalComponent', () => {
  let component: OTModalComponent;
  let fixture: ComponentFixture<OTModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OTModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OTModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
