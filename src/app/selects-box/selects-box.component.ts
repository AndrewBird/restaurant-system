import { Component, OnInit, Input } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { AssignStaffModalComponent } from '../assign-staff-modal/assign-staff-modal.component';
import { CoversModalComponent } from '../covers-modal/covers-modal.component';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/pairwise';
import { Router, Event, NavigationStart } from '@angular/router';
import { StateGuardService } from '../state-guard.service';

@Component({
  selector: 'app-selects-box',
  templateUrl: './selects-box.component.html',
  styleUrls: ['./selects-box.component.css']
})
export class SelectsBoxComponent implements OnInit {
  @Input() deliveryType: number;
  // @ViewChild('coversModal') public coversModal: ModalDirective;
  // , TemplateRef, ViewChild
  public modalRef: BsModalRef;
  public subscriptions: Subscription[] = [];
  public messages: string[] = [];
  private activePhases = [];
  currentPlace: any;
  constructor(private modalService: BsModalService, private router: Router, private stateGuard: StateGuardService) {  }

  ngOnInit() {
    this.selectPlace(this.places[0]);
    this.places.forEach(place => {
      place.units.forEach(unit => {
        if (unit["activePhase"]) {
          this.startActivePhase(unit);
        }
      });
    });
    this.router.events.subscribe((event) => {
        if (event instanceof NavigationStart) {
          this.stateGuard.entrust('selects-box.component:places', this.places);
        }
    });
  }
  public openAssignStaffModal() {
    var chosenUnits = [],
        chosenPlaces = this.currentPlace.units.filter(unit => {
            if (unit.chosen) {
              chosenUnits = [...chosenUnits, ...unit.tables];
            }

            return unit.chosen;
        });
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'assign') {
        this.assignStaff(this.modalRef.content.assigny);
      }
    }));
    this.modalRef = this.modalService.show(AssignStaffModalComponent, 
                    {class: 'spartan-modal assign-stuff'});
    this.modalRef.content.placesToAssaign = chosenPlaces;
    this.modalRef.content.unitsStr = chosenUnits.join(", ");
    this.modalRef.content.assignStaff = this.assignStaff;
    this.modalRef.content.decision = "";
    this.modalRef.content.assigny = "";
    
  }
  public openCoversModal(targetUnit) {
    var self = this;

    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {
        targetUnit.covers = this.modalRef.content.coversAmount;
        targetUnit.visitors = [];

        for (var i = targetUnit.covers; i > 0; i--) {
          targetUnit.visitors.push(Object.assign({}, {dishes: [[],[]]}));
        }

        this.startActivePhase(this.modalRef.content.targetUnit);
        this.stateGuard.entrust('selects-box.component:currentUnit', targetUnit);
        setTimeout(() => {this.router.navigate(['/food-ordering'])}, 0);
      }
      this.unsubscribe();
    }));

    this.modalRef = this.modalService.show(CoversModalComponent, 
                    {class: 'spartan-modal covers'});
    this.modalRef.content.decision = "";
    this.modalRef.content.coversAmount = "";
    this.modalRef.content.targetUnit = targetUnit;
  }
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
  public startActivePhase(unit) {
    var ticker, previousePhasing = null,
        timeOut = setTimeout(function () {
          autoSwitchPhase(unit);
        }, 600000),
        autoSwitchPhase = function (unit) {
          unit.activePhase.name = "red";
        },
        timerPipe = function (time) {
          var hours = Math.floor(time / 3600), 
          minutes = Math.floor((time - hours * 3600) / 60),
          seconds = time - hours * 3600 - minutes * 60;
      
          return ("00" + hours+ ":").substr(-3) + ("00" + minutes + ":").substr(-3) + ("00" + seconds).substr(-2);
        },
        runTimer = function () {
          var theUnit = unit;
          return function () {
            ticker = setInterval(function () {
              var time = Math.round((new Date().valueOf() - theUnit.activePhase.timeStamp) / 1000);
              theUnit.activePhase.timerText = timerPipe(time);
            }, 1000);
            unit.activePhase.ticker = ticker;
          } 
        };

      if (unit.activePhase) {
        previousePhasing = unit.activePhase;
        this.interruptActivePhase(unit);
      }
      unit.activePhase = {
        name: previousePhasing ? previousePhasing.name : "green",
        timeOut,
        timeStamp: previousePhasing ? previousePhasing.timeStamp : new Date().valueOf(),
        timerText: previousePhasing ? previousePhasing.timerString : "00:00:00"
      };
      runTimer()();
  }

  assignStaff(assigny) {
    this.currentPlace.units.forEach(unit => {
      if (unit.chosen) {
        unit.assigny = assigny;
      }
    });
  }
  private interruptActivePhase (unit) {
    clearInterval(unit.activePhase.ticker);
    clearTimeout(unit.activePhase.timeOut);
    delete unit.activePhase;
  }
  getEngagment (place) {
    var engaged = 0, all = 0;

    place.units.forEach(unit => {
      engaged += unit.assigny === '' ? 0 : unit.tables.length;
      all += unit.tables.length;
    });

    return engaged + "/" + all;
  }
  public places = this.stateGuard.obtain('selects-box.component:places') || [];
 
  public selectPlace (newPlace) {
    this.places.forEach(place => {
      place.chosen = false;
    });
    newPlace.chosen = true;
    this.currentPlace = newPlace;
  }
  toggleMergeState(newState) {
    this.currentPlace.mergeOpen = newState;
  }
  handleUnitClick(unit) {
    unit.chosen = !unit.chosen;
    if (!this.currentPlace.mergeOpen) {
      if (unit.covers) {
        this.stateGuard.entrust("selects-box.component:currentUnit", unit);
        setTimeout(() => {this.router.navigate(['/food-ordering'])}, 0);
      } else {
        this.openCoversModal(unit);
      }
    }
  }

  merge() {
    let mergedUnit = {tables: [], assigny: '', chosen: true},
        result = [];

    this.currentPlace.units.forEach(unit => {
      if (unit.chosen) {
        if (unit.activePhase) {
          this.interruptActivePhase(unit);
        }
        mergedUnit.tables = [...mergedUnit.tables, ...unit.tables];
      } else {
        result.push(unit);
      }
    });
    if (mergedUnit.tables.length <  2) {
      return;
    }
    mergedUnit.tables.sort((a, b) => {
      return a > b ? 1 : -1;
    });
    result.push(mergedUnit);
    result = result.sort((a: any, b: any) => {
      return a.tables[0] > b.tables[0] ? 1 : -1;
    });

    this.currentPlace.units = result;
    return result;
  }
  unmerge() {
      let result = [];
          
      this.currentPlace.units.forEach(unit => {
        if (unit.chosen && unit.tables.length > 1) {
            unit.tables.forEach(num => {
              result.push({tables: [num], assigny: unit.assigny, chosen: true});
            });
        } else {
          result.push(unit);
        }
        if (unit.chosen && unit.activePhase) {
          this.interruptActivePhase(unit);
        }
      });
      result = result.sort((a: any, b: any) => {
        return a.tables[0] > b.tables[0] ? 1 : -1;
      });

      this.currentPlace.units = result;
      return result;
  }

}
