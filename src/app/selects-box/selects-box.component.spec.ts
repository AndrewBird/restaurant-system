import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectsBoxComponent } from './selects-box.component';

describe('SelectsBoxComponent', () => {
  let component: SelectsBoxComponent;
  let fixture: ComponentFixture<SelectsBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectsBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectsBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
