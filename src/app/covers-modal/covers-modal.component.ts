import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
  selector: 'app-covers-modal',
  templateUrl: './covers-modal.component.html',
  styleUrls: ['./covers-modal.component.less']
})
export class CoversModalComponent implements OnInit, AfterViewInit {
  decision: string;
  coversAmount: number;
  targetUnit: any;
  modal = {covers: null};
  @ViewChild('coversInput') coversInput;
  constructor(public modalRef: BsModalRef) { }
  
  ngOnInit() {

  }
  ngAfterViewInit () {
    document.querySelector("#covers-amount")["focus"]();
  }

}
