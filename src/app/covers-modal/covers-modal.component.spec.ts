import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoversModalComponent } from './covers-modal.component';

describe('CoversModalComponent', () => {
  let component: CoversModalComponent;
  let fixture: ComponentFixture<CoversModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoversModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoversModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
