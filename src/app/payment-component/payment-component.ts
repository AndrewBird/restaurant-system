import { Component, OnInit, Output, EventEmitter, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { StateGuardService } from '../state-guard.service';
import { Messenger } from '../messenger.service';


import { ScreenKeysDirective } from '../screen-keys.directive';
@Component({
  selector: 'app-payment-component',
  templateUrl: './payment-component.html',
  styleUrls: ['./payment-component.css']
})
export class PaymentComponent implements OnInit {
  paymentWay: string = "cash";
  currentSlideIndex: number = 0;
  currentSlide: any;
  comingSlide: any;
  slider: any;
  message: any = {text: ''};
  money: string;
  invoice = {
    amount: 1301
  }

  constructor(private router: Router,
              private stateGuard: StateGuardService,
              private messageService: Messenger) {}

  reactOnKeys (parcel) {
    switch (parcel.action) {
      case ('bill'):
        console.log('bill');
      break;
      case ('print'):
        console.log('print');
      break;
    }
  }

  compute () {
    return this.invoice.amount - (parseInt(this.money) || 0);
  }
  movesDetected () {
    this.messageService.sendMessage(this.compute() + "");
  }
  letsPayWith (way) {
    this.paymentWay = way;
    setTimeout(() => {
      this.messageService.sendMessage(this.compute() + "");
    }, 0)
  }
  closePayment () {
    this.router.navigate(['/food-ordering']);
  }
  ngOnInit() {
    let self = this;
    document.querySelector("#payment").addEventListener("keyup", function (event) {
      if (event["which"] === 27) {
        self.closePayment();
      }
    });
    this.slider = document.querySelector("#payment-options");
    this.slideIn(document.querySelector("#payment-options .item.active"), "left");
    this.messageService.sendMessage(this.money);
  }
  slideTo (newIndex: number, slides) {
    if (newIndex === this.currentSlideIndex) {return;}
    let direction = this.currentSlideIndex > newIndex ? "left" : "right";

    this.slider = slides; 
    this.currentSlide = slides.children[this.currentSlideIndex];
    this.currentSlideIndex = newIndex;
    while (slides.querySelector(".item .active")) {
      slides.querySelector(".item .active").classList.remove('active');
    }
    this.comingSlide = slides.children[newIndex];
    this.comingSlide.classList.add("active");
    this.slideOut(this.currentSlide, direction);
    this.slideIn(this.comingSlide, direction);
  }
  private slideIn (el, dir) {
    if (!el) {
        console.warn("No elemen provided to slide in. El is: ", el);
        return;
    }
    var c = this.slider.clientWidth,
        time = 4096 / c,
        prep = dir === "left" ? "-" : "";
		
		el.style.left = prep + c;
		function run() {
			c -= 16;
			if (c >= 0){
				setTimeout(function() {
					el.style.left = prep + c + "px";
					run();
				}, time);
			} else {
				el.style.left = "0px";
			}
		}
    run();
  }
  private slideOut(el, dir){
    if (!el) {
      console.warn("No elemen provided to slide out. El is: ", el);
      return;
    }
		var screen = this.slider.clientWidth, 
        time = 4096 / screen,
        c = 1, prep = dir === "left" ? "" : "-";
		
		function run () {
			c += 16;
			if (c <= screen) {
				setTimeout(function() {
					el.style.left = prep + c + "px";
					run();
				}, time);
			} else {
				el.style.left = prep + 100 + "%";
				return;
			}
		}
    run();
  }
}
