import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParkModalComponent } from './park-modal.component';

describe('ParkModalComponent', () => {
  let component: ParkModalComponent;
  let fixture: ComponentFixture<ParkModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParkModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
