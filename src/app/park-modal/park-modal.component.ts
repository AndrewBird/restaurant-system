import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { StateGuardService } from '../state-guard.service';

@Component({
  selector: 'park-modal',
  templateUrl: './park-modal.component.html',
  styleUrls: ['./park-modal.component.less']
})
export class ParkModalComponent implements OnInit {
  constructor(public modalRef: BsModalRef, private stateGuard: StateGuardService) { }
  decision: string;
  offUnits: Array<any> = [];
  selectedHoldUnit;
  ngOnInit() {
    this.offUnits = this.stateGuard.obtain('basket.component:offUnits');
  }
}
