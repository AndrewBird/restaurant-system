import { TestBed, inject } from '@angular/core/testing';

import { StateGuardService } from './state-guard.service';

describe('StateGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [StateGuardService]
    });
  });

  it('should be created', inject([StateGuardService], (service: StateGuardService) => {
    expect(service).toBeTruthy();
  }));
});
