import { Component, OnInit, Output, EventEmitter, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { ActivatedRoute } from '@angular/router';
import { GiftCardModalComponent } from '../gift-card-modal/gift-card-modal.component';
import { Subscription } from 'rxjs/Subscription';
import { OTModalComponent } from '../otmodal/otmodal.component';
import { StateGuardService } from '../state-guard.service';
import { ParkModalComponent } from '../park-modal/park-modal.component';
import { Router } from '@angular/router';
import { OffersModalComponent } from '../offers-modal/offers-modal.component';
import { DiscountsModalComponent } from '../discounts-modal/discounts-modal.component';

@Component({
  selector: 'app-options-box',
  templateUrl: './options-box.component.html',
  styleUrls: ['./options-box.component.less']
})
export class OptionsBoxComponent implements OnInit {
  @Output() paymentState: EventEmitter<boolean> = new EventEmitter();
  offUnits;
  public modalRef: BsModalRef;
  isItFoodOrderingPage: boolean;
  buttonsEnabled: any;
  guestsEditorOn: boolean = false;
  constructor(private modalService: BsModalService,
              private route: ActivatedRoute,
              private stateGuard: StateGuardService,
              private router: Router) {
    route.paramMap.subscribe(
      params => {
        this.isItFoodOrderingPage = this.route.snapshot.url[0].path === "food-ordering";
        this.buttonsEnabled = {
          tables: this.isItFoodOrderingPage,
          covers: this.isItFoodOrderingPage,
          hold: this.isItFoodOrderingPage,
          park: true,
          offers: true,
          quickAction: this.isItFoodOrderingPage,
          edit: this.isItFoodOrderingPage,
          discount: this.isItFoodOrderingPage,
          gift: this.isItFoodOrderingPage,
          ot: this.isItFoodOrderingPage,
          payment: this.isItFoodOrderingPage
        }
      }
    );
    this.isItFoodOrderingPage = this.route.snapshot.url[0].path === "food-ordering";
  }

  public currentUnit = this.stateGuard.obtain("selects-box.component:currentUnit") || [];
  public subscriptions: Subscription[] = [];
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }

  
  showGiftCardModal () {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {

      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(GiftCardModalComponent, {class: 'common-modal'});
    this.modalRef.content.decision = "";
  }
  showDiscountsModal () {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {

      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(DiscountsModalComponent, {class: 'common-modal'});
    this.modalRef.content.decision = "";
  }
  showParkModal () {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'select') {
        this.currentUnit = this.modalRef.content.selectedHoldUnit;
        this.stateGuard.entrust("selects-box.component:currentUnit", this.modalRef.content.selectedHoldUnit)
        this.router.navigate(['food-ordering']);
      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(ParkModalComponent, {class: 'park-modal centred-modal'});
    this.modalRef.content.decision = "";
  }
  showOTModal () {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'send') {
        this.modalRef.content.artefacts.unit.activePhase.name = "green";
        this.router.navigate(['place-selection'], this.modalRef.content.artefacts.choosenVisitors);
      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(OTModalComponent, {class: 'centred-modal'});
    this.modalRef.content.unit = this.stateGuard.obtain("selects-box.component:currentUnit") || [];
    this.modalRef.content.decision = "";
  }
  putOnHold (unit) {
    /*unit.visitors.forEach(visitor => {
      if (visitor.isOnHold) { return; }
      var offUnit = Object.assign({}, this.currentUnit); 
      offUnit.visitors = [visitor];
  
      this.stateGuard.amend('basket.component:offUnits', function (offUnits) {
        var newOffUnit = offUnit;
        offUnits.push(offUnit);
      });
      visitor.isOnHold = true;
    });
    this.router.navigate(['place-selection']);
    */
    unit.visitors.forEach((seat, index) => {
      /*if (seat.isOnHold) {
        unit.visitors.splice(index, 1);
      }*/
      seat.isOnHold = true;
    });
    if (unit.isOnHold) { return; }
    unit.isOnHold = true;
    unit.isOnHold = true;
    this.stateGuard.amend('basket.component:offUnits', function (offUnits) {
      offUnits.push(unit);
    });
    this.router.navigate(['place-selection']);
  }
  ngOnInit() {

    this.offUnits = this.stateGuard.obtain('basket.component:offUnits');
  }
  showOffersModal () {
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {

      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(OffersModalComponent, {class: 'offers-modal common-modal'});
    this.modalRef.content.decision = "";
  }
  openPayment() {
    this.paymentState.emit(true);
  }
  editGuestAmount (direction) {
    var targetIndex = 0, weakest;

    this.guestsEditorOn = false;
    if (direction === "+") {
      this.currentUnit.visitors.push({dishes: [[],[]]});
    } else {
      if (!this.currentUnit.visitors.length) {
        return;
      }
      weakest = this.currentUnit.visitors[0].dishes.length;
      this.currentUnit.visitors.forEach((visitor, index) => {
        if (!visitor.dishes.length) {
          targetIndex = index;
          return;
        }
        let dishesNum = visitor.dishes.length;
        if (weakest >= dishesNum) {
          weakest = dishesNum;
          targetIndex = index;
        }
      });
      this.currentUnit.visitors.splice(targetIndex, 1);
    }
    this.currentUnit.covers = this.currentUnit.visitors.length;
  }

}
