import { Component } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

import { LoginFormComponent } from '../login-form/login-form.component';
@Component({
  selector: 'app-demo-modal-service-static',
  templateUrl: './demo-modal-service-static.component.html',
  styleUrls: ['./demo-modal-service-static.component.css']
})
export class DemoModalServiceStaticComponent {
  bsModalRef: BsModalRef;
  public config = {
    animated: true,
    keyboard: false,
    backdrop: true,
    ignoreBackdropClick: true
  };
  constructor(private modalService: BsModalService) {}
 
  public openModalWithComponent() {
    var configuration = Object.assign({}, this.config, {class: 'centred-modal'});
    this.bsModalRef = this.modalService.show(LoginFormComponent, configuration);
    this.bsModalRef.content.title = 'Modal with component';
  }
}
