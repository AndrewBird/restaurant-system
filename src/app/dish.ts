export interface Dish {
    name: string
    amount: number
    price: number
    expanded: boolean
    course: string
    plus?: string
    extraFee?: number
    discount?: number
    note?: string,
    discountUnits?: string
}
