import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiscountsModalComponent } from './discounts-modal.component';

describe('DiscountsModalComponent', () => {
  let component: DiscountsModalComponent;
  let fixture: ComponentFixture<DiscountsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiscountsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiscountsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
