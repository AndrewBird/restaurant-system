import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { StateGuardService } from '../state-guard.service';

@Component({
  selector: 'app-discounts-modal',
  templateUrl: './discounts-modal.component.html',
  styleUrls: ['./discounts-modal.component.less']
})
export class DiscountsModalComponent implements OnInit {
  decision: string;
  constructor(public modalRef: BsModalRef, private stateGuard: StateGuardService) { }
  isPercentsDiscount: boolean = true;
  currency: string = "$";
  ngOnInit() {
  }

}
