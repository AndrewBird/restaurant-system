import { Component, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';

@Component({
    selector: 'login-form',
    templateUrl: './login-form.component.html',
    styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent {
    loginStep: number = 1;
    //@Input() modalRef;

    constructor(private http: HttpClient, public modalRef: BsModalRef) {

    }

    onLoggedin() {
        console.log('isLoggedin', 'true');
    }
    requestOPT (loginForm) {
        this.loginStep = 2;
    }
    run(form) {
        console.log(form);
        if (form.valid) {
            this.http.post("/admin/login", JSON.stringify(form.value)).subscribe(response => {
                console.log(response);
            }, reason => {
                console.error(reason);
            });
        }
    }
}


