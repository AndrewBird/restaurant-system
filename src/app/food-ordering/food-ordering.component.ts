import { Component, OnInit, Output, EventEmitter, ViewChild, AfterViewInit  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BasketComponent } from '../basket/basket.component';

@Component({
  selector: 'app-food-ordering',
  templateUrl: './food-ordering.component.html',
  styleUrls: ['./food-ordering.component.less']
})
export class FoodOrderingComponent implements OnInit, AfterViewInit  {
  isPaymentOpen: boolean;
  constructor(private route: ActivatedRoute) {
    route.paramMap.subscribe(
      params => {
        this.isPaymentOpen = this.route.snapshot.params.payment === "payment";
      }
    );
  }
  @ViewChild(BasketComponent) basketComponent: BasketComponent;
	@Output() optionsState: EventEmitter<boolean> = new EventEmitter();
  ngAfterViewInit() {
    
  }
  ngOnInit() {
    this.optionsState.emit(true);
  }
  reactOnAddingDish (parcel) {
    console.log(parcel);
    this.basketComponent.addToBasket(parcel);
  }
}
