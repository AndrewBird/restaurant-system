import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Dish } from '../dish';

@Component({
  selector: 'app-inventory-modal',
  templateUrl: './inventory-modal.component.html',
  styleUrls: ['./inventory-modal.component.less']
})
export class InventoryModalComponent implements OnInit {
  decision: string;
  dish: Dish = {
    name: "",
    amount: 0,
    price: 0,
    expanded: false,
    course: ""
  };
  constructor(public modalRef: BsModalRef) { }

  ngOnInit() {
  }

}
