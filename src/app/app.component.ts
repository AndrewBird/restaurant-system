import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { StateGuardService } from './state-guard.service';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  delType: number = 1;
  modalRef: BsModalRef;
  constructor(private route: ActivatedRoute,
              private stateGuard: StateGuardService,
              private modalService: BsModalService) {
  }
  visitorArtefacts = {
    name: "Neville Smith",
    lastVisit: new Date(),
    lastCheck: 20000,
    lastCheckCurrency: "RP",
    email: "neville.smith@gmail.com",
    phoneNumber: "0938102383",
    points: 1000,
    place: "here"
  }
  public subscriptions: Subscription[] = [];
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
  ngOnInit() {
    this.stateGuard.entrust("selects-box.component:currentUnit", this.hardcodedUnit);
    this.stateGuard.entrust("selects-box.component:places", this.hardcodedLocations);
    this.stateGuard.entrust('basket.component:offUnits', []);
  }
  
  openCustomerInfoModal() {
    this.modalRef = this.modalService.show(CustomerInfoComponent, {class: 'common-modal'});
    this.modalRef.content.decision = "";
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'select') {
        console.log(this.modalRef.content.customerInfo.value);
      }
      this.unsubscribe();
    }));
  }
  hardcodedUnit = {
    location: "Garden",
    tables: [1],
    covers: 2,
    activePhase: {
      name: "green",
      timeStamp: new Date().valueOf(),
      timerText: "00:00:00"
    },
    visitors: [{
      dishes: [
        [{
          name: "Creamy Spinach Bake",
          amount: 1,
          price: 159,
          plus: "With Extra Cheese &...",
          extraFee: 50,
          course: "c1"
        },
        {
          name: "Sesame-crusted Tuna",
          amount: 2,
          price: 111,
          course: "c1"
        }],
        [{
            name: "Garlic Bread Exotica",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1"
          },
          {
            name: "Sesame-crusted Tuna",
            amount: 2,
            price: 111,
            course: "c1"
          }
        ]
      ]
    },
    {
      dishes: [
        [{
          name: "Creamy Spinach Bake",
          amount: 1,
          price: 159,
          plus: "With Extra Cheese &...",
          extraFee: 50,
          course: "c1"
        },
        {
          name: "Sesame-crusted Tuna",
          amount: 2,
          price: 111,
          course: "c1"
        }],
        [{
            name: "Garlic Bread Exotica",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1"
          },
          {
            name: "Sesame-crusted Tuna",
            amount: 2,
            price: 111,
            course: "c1"
          }
        ]
      ]
    }]
  };
  hardcodedLocations = [
    {name: "Garden",
     chosen: true,
     units: [this.hardcodedUnit,
              {location: "Garden", tables: [2], assigny: '', chosen: false},
              { location: "Garden",
                tables: [3, 4],
                assigny: '',
                chosen: false,
                covers: 2,
                visitors: [{dishes: [
                  [{
                    name: "Sandwiches & Wraps Recipes",
                    amount: 1,
                    price: 29,
                    course: "c1"
                  }], []
                ]}, {dishes: [
                  [{
                    name: "Potato, Squash & Goat Cheese Gratin",
                    amount: 1,
                    price: 77,
                    course: "c1"
                  }], []
                ]}],
                activePhase: {
                  name: "green",
                  timeStamp: 1505650982528,
                  timerText: "00:00:00"
                }
              }],
      mergeOpen: false
    },
    {name: "Patio",
     chosen: false,
     units: [{location: "Patio", tables: [5], assigny: '', chosen: false, visitors: []},
              {location: "Patio", tables: [6], assigny: '', chosen: false, visitors: []},
              {location: "Patio", tables: [7], assigny: '', chosen: false, visitors: []},
              {location: "Patio", tables: [8], assigny: '', chosen: false, visitors: []}],
      mergeOpen: false
    },
    {name: "1 Floor",
     chosen: false,
     units: [{location: "1 Floor", tables: [9], assigny: '', chosen: false, visitors: []},
              {location: "1 Floor", tables: [10], assigny: '', chosen: false, visitors: []},
              {location: "1 Floor", tables: [11], assigny: '', chosen: false, visitors: []}],
      mergeOpen: false
    },
    {name: "2 Floor",
     chosen: false,
     mergeOpen: false,
     units: [{location: "2 Floor", tables: [12], assigny: '', chosen: false, visitors: []},
              {location: "2 Floor", tables: [13], assigny: '', chosen: false, visitors: []},
              {location: "2 Floor", tables: [14], assigny: '', chosen: false, visitors: []}]
    },
    {name: "3 Floor",
     chosen: false,
     mergeOpen: false,
     units: [{location: "3 Floor", tables: [15], assigny: '', chosen: false, visitors: []},
              {location: "3 Floor", tables: [16], assigny: '', chosen: false, visitors: []},
              {location: "3 Floor", tables: [17], assigny: '', chosen: false, visitors: []}]
    },
    {name: "4 Floor",
     chosen: false,
     mergeOpen: false,
     units: [{location: "4 Floor", tables: [18], assigny: '', chosen: false, visitors: []},
              {location: "4 Floor", tables: [19], assigny: '', chosen: false, visitors: []},
              {location: "4 Floor", tables: [20], assigny: '', chosen: false, visitors: []}]
    },
    {name: "5 Floor",
     chosen: false,
     mergeOpen: false,
     units: [{location: "5 Floor", tables: [21], assigny: '', chosen: false, visitors: []},
              {location: "5 Floor", tables: [22], assigny: '', chosen: false, visitors: []},
              {location: "5 Floor", tables: [23], assigny: '', chosen: false, visitors: []}]
    },
    {name: "6 Floor",
     chosen: false,
     mergeOpen: false,
     units: [{location: "6 Floor", tables: [24], assigny: '', chosen: false, visitors: []},
              {location: "6 Floor", tables: [25], assigny: '', chosen: false, visitors: []},
              {location: "6 Floor", tables: [26], assigny: '', chosen: false, visitors: []}]
    }
  ]
}