import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BasketComponent } from '../basket/basket.component';
import { Dish } from '../dish';
import { Messenger } from '../messenger.service';
import { Subscription } from 'rxjs/Subscription';
import { BsModalRef } from 'ngx-bootstrap/modal/modal-options.class';
import { BsModalService } from 'ngx-bootstrap/modal';
import { OffersModalComponent } from '../offers-modal/offers-modal.component';
import { StateGuardService } from '../state-guard.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.less']
})

export class MenuComponent implements OnInit {
  @Output() addingDish: EventEmitter<Dish> = new EventEmitter();
  displayAutocomplete: boolean = false;
  public subscriptions: Subscription[] = [];
  public modalRef: BsModalRef;
  private currentUnit = this.stateGuard.obtain("selects-box.component:currentUnit") || [];
  constructor (private messenger: Messenger,
               private modalService: BsModalService,
               private stateGuard: StateGuardService) {
    
  }
  private unsubscribe() {
    this.subscriptions.forEach((subscription: Subscription) => {
      subscription.unsubscribe();
    });
    this.subscriptions = [];
  }
  showOffersModal (dish) {
    if (!dish.offer) { return; }
    this.subscriptions.push(this.modalService.onHide.subscribe(() => {
      if (this.modalRef.content.decision === 'done') {

      }
      this.unsubscribe();
    }));
    this.modalRef = this.modalService.show(OffersModalComponent, {class: 'offers-modal common-modal'});
    this.modalRef.content.decision = "";
  }
  reactOnSearchChange (event, searchField) {
    event.preventDefault();
    this.displayAutocomplete = !!searchField.value;
    this.messenger.sendMessage(searchField.value);
  }
  countGrabbed(dish) {
    var counter = 0;
    this.currentUnit.visitors.forEach(visitor => {
      visitor.dishes.forEach(course => {
        course.forEach(visitorsDish => {
          if (visitorsDish.name === dish.name) {
            counter++;
          }
        });
      });
    });
    return counter;
  }
  clearMessage(): void {
      this.messenger.clearMessage();
  }

  showKeyboard: boolean = false;
  currentDishesGroup;
  dishesModel;
  targetVisitor;
  searchParam = {text: ""}
  menuChapters = [{name: "Favourite", active: false},
                  {name: "Starters", active: false},
                  {name: "Dal Mal", active: false},
                  {name: "Vegetable", active: false},
                  {name: "Roti", active: false},
                  {name: "Pizza", active: false},
                  {name: "Cake", active: false},
                  {name: "Cold Drink", active: false}];
  addDish(dish: Dish) {
    this.addingDish.emit(dish);
  }
  runSearch (searchParams) {
    var params = searchParams.split(" "),
        results = [],
        searchBuffer = this.dishesModel[0];
        searchBuffer.dishes = [];

    this.currentDishesGroup = "search";
    this.dishesModel.forEach(dishesGroup => {
      dishesGroup.dishes.forEach(dish => {
        params.forEach(param => {
          if (dish.name.toLowerCase().indexOf(param.toLowerCase()) >= 0){
            results.push(dish);
          }
        });
      });
    });
    searchBuffer.dishes = results;
  }
  reactOnKeyboardAction(parcel, searchField) {
    var caretPos = this.getCaretPosition(searchField),
        currentVal = searchField.value;
      if (typeof parcel === "string") {
        searchField.value = currentVal.substring(0, caretPos) + parcel + currentVal.substring(caretPos, currentVal.length);
        searchField.setSelectionRange(caretPos + 1, caretPos + 1);
        searchField.focus();
      } else {
        if (parcel === 1) {
          this.runSearch(currentVal)
        } else if (parcel === -1) {
          searchField.value = currentVal.substring(0, caretPos - 1) + currentVal.substring(caretPos, currentVal.length);
          searchField.setSelectionRange(caretPos - 1, caretPos - 1);
          searchField.focus();
        } else {
          this.showKeyboard = false;
        }
      }
  }
  private getCaretPosition (oField) {
      var iCaretPos = 0;
  
      if (document["selection"]) {
        oField.focus();
        var oSel = document["selection"].createRange();
        oSel.moveStart('character', -oField.value.length);
        iCaretPos = oSel.text.length;
      } else if (oField.selectionStart || oField.selectionStart == '0') {
        iCaretPos = oField.selectionStart;
      }

      return iCaretPos;
  }
  switchDishesGroup (chapter) {
    this.currentDishesGroup = chapter;
  }
  reactOnAautocomplete (parcel) {
    console.log(parcel);
    this.displayAutocomplete = false;
  }
  ngOnInit() {
    this.dishesModel = [
      {
        name: "search",
        dishes: []
      },
      {
        name: "Favourite",
        dishes: [
          {
            name: "Garlic Bread Exotica",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1",
            offer: [],
            discount: 21
          },
          {
            name: "Roasted Potatoes, Radishes & Fennel",
            amount: 1,
            price: 159,
            plus: "With Lemon-Brown Butter Sauce",
            extraFee: 50,
            course: "c1",
            offer: [],
            discount: 21
          },{
            name: "Creamy Spinach Bake",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1",
            discount: 21
          },
          {
            name: "Sesame-crusted Tuna",
            amount: 1,
            price: 111,
            course: "c1",
            offer: []
          },
          {
            name: "Baked Potato Casserole",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c2"
          }]
      }, {
        name: "Starters",
        dishes: [
          {
            name: "Garlicky Roasted Broccoli",
            amount: 1,
            price: 49,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1"
          },
          {
            name: "Crispy Oven-Baked Asparagus Fries",
            amount: 1,
            price: 111,
            course: "c1"
          }]
      }, {
        name: "Dal Mal",
        dishes: [
          {
            name: "Hasselback Tater Tots",
            amount: 1,
            price: 190,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1"
          },
          {
            name: "Zucchini and Squash with Thyme and Feta",
            amount: 1,
            price: 43,
            course: "c2"
          }]
      }, {
        name: "Vegetable",
        dishes: [
          {
            name: "Corn with Lemon-Brown Butter Dressing",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c1"
          },
          {
            name: "Sesame Snow Peas",
            amount: 1,
            price: 111,
            course: "c1"
          }]
      },
      {
        name: "Roti",
        dishes: [
          {
            name: "Couscous & Goat Cheese Stuffed Tomatoes",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 50,
            course: "c2"
          }]
      }, {
        name: "Pizza",
        dishes: [
          {
            name: "Roasted Mushrooms with Herbs",
            amount: 1,
            price: 76,
            course: "c2"
          }]
      }, {
        name: "Cake",
        dishes: [
          {
            name: "Molten Peanut Butter Bundt Cake",
            amount: 1,
            price: 26,
            course: "c2"
          },
          {
            name: "Apple Cinnamon Fruit Roll-Ups",
            amount: 1,
            price: 37,
            course: "c2"
          },
          {
            name: "Rainbow Ice Cream Cake",
            amount: 1,
            price: 70,
            course: "c2"
          },
          {
            name: "Banana Pudding Ice Cream Cake",
            amount: 1,
            price: 47,
            course: "c2"
          }],
      }, {
        name: "Cold Drink",
        dishes: [
          {
            name: "White Walkers Milkshake Shots",
            amount: 1,
            price: 159,
            plus: "With Extra Cheese &...",
            extraFee: 62,
            course: "c2"
          },
          {
            name: "Piña Colada Protein Smoothie",
            amount: 1,
            price: 45,
            course: "c2"
          },
          {
            name: "Salted Caramel-Pretzel Milkshake",
            amount: 1,
            price: 56,
            course: "c2"
          }]
      }
    ];
    this.currentDishesGroup = "Favourite";
  }

}