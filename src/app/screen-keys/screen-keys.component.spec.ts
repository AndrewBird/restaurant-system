import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScreenKeysComponent } from './screen-keys.component';

describe('ScreenKeysComponent', () => {
  let component: ScreenKeysComponent;
  let fixture: ComponentFixture<ScreenKeysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScreenKeysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScreenKeysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
